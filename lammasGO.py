#================================================
#
# Quick start script for LAMMAS
# This scirpt is for quick and easy estimations
#
#================================================

import numpy as np
import lammas as la
import matplotlib.pyplot as plt

import os
import sys
import argparse

parser = argparse.ArgumentParser(description='lammasGO a quick start script for lammas')

parser.add_argument("-f", "--file", help = "Use file as input.", default="", type = str)
parser.add_argument("-o", "--output", help = "Manual output directory", default="./outputs", type = str)
parser.add_argument("-l", "--logmass", help = "Toggles if masses are in logscale", action="store_true")
parser.add_argument("-v", "--verbose", help = "Toggles verbosity", action="store_true")
parser.add_argument("-m", "--masses", help = "Manual mass input", default =[], type = float, nargs = '*')
parser.add_argument("-z", "--redshift", help="Cluster redshift.", default = -1, type = float)


args = parser.parse_args()

file = args.file
output = args.output
logmass = args.logmass
masses = args.masses
z = args.redshift
verbose = args.verbose

# General setting up of the work environement

if len(file) > 0:
    #masses = np.loadtxt(file)
    with open(file, 'r') as f:
        masses = [float(mass) for mass in f.readline().split()]
        z = float(f.readline())

if len(masses) < 2:
    raise ValueError("At least two mass measuremnts must be specified in the order M_200c, M_500c, M_2500c")

if z < 0:
    raise ValueError(f"Invalid redshift value, z = {z:.2f}. z must be in [0,2]")
if logmass:
    masses = 10**np.array(masses)

if not os.path.isdir(output):
    os.system(f"mkdir {output}")

# Preping lammas using Planck 2018 cosmology

H0 = 67.77
Om = 0.307115
OL = 0.692885

# Define the Cosmo Clock
clock = la.CosmoClock(H0,Om,OL)
# Define the Merger Timer
if len(masses) == 2:
    timer = la.MergerTimer(masses[0], masses[1], z_obs = z, clock = clock, verbose = verbose, prior = 'flat-a')
elif len(masses) == 3:
    timer = la.MergerTimer(masses[0], masses[1], masses[2], z_obs = z, clock = clock, verbose = verbose, prior = 'flat-a')
else:
    raise ValueError('Something went wrong... the masses are ill defined.')
# Run the code!
res = timer.run()

pval = res['pval']
BF1D = res['BF1D']

latex = timer.latexise_results()

with open(output+'/Sumary_Statistics.dat','w') as f:
    print('Sumarised lammasGO output file\n', file = f)

    print(f'1 sparsity:\n\ns_{{200,500}} = {timer._s200_500:.3f}\n', file = f)
    print(f'p-value = {pval:.3}', file = f)
    print(f"K_1 = {BF1D:.3}", file = f)
    if pval > 0.1:
        print('\n# /!\ Warning such a high p-value can indicate that the 1 sparsity Bayesian estimates may be biased.', file = f)
        print('# Care must be taken when quoting the 1 sparsity Bayes factor and Most likely parameters.', file = f)
        print('# It is recommended to check full posterior distributions and to fall back on 3 sparsity estimates if possible.\n', file = f)

    for i in range(3):
        print(latex[i], file = f)

    if len(masses) == 3:
        BF3D = res['BF3D']
        print(f'\n\n3 sparsities:\n', file = f)
        print(f"s_{{200,500}} = {timer._s[0]:.3f} " , file = f)
        print(f"s_{{200,2500}} = {timer._s[1]:.3f}", file = f)
        print(f"s_{{500,2500}} = {timer._s[2]:.3f}\n", file = f)

        print(f"K_3 = {BF3D:.3}\n", file = f)


        for i in range(3,6):
            print(latex[i], file = f)

ax = timer.plot_posteriors()
plt.savefig(output + '/Posterior_distributions.pdf', bbox_inches = 'tight')