import numpy as np
import scipy. stats as st
import matplotlib.pyplot as plt

from scipy.integrate import simpson as simps
from scipy.integrate import trapezoid as trapz
from scipy.integrate import quad, dblquad
from scipy.optimize import curve_fit, root
from scipy.interpolate import interp1d, griddata
from scipy.special import beta

import h5py
import emcee
import os

package_directory = os.path.dirname(os.path.abspath(__file__))

class CosmoClock:
    '''
    CosmoClock class. Used for basic cosmological calculations related to time
    
    Parameters:
    ----------
    H0: (float) Hubble constant at z = 0
    Om0: (float) Matter density parameter at z = 0
    OL0: (float) Dark Energy density parameter at z = 0
    
    Methods:
    ----------
    - get_cosmo_params : Returns cosmological parameters
    
    - set_cosmo_params : Sets cosmological parameters
    
    - O : Returns the matter density parater at a given scale factor
    
    - Hubble_a : Returns the Hubble parameter at a given scale factor
    
    - t_of_a : Returns cosmic time in Myr as a function of the scale factor 
    
    - t_of_a_fast : Retruns cosmic time in Myr as a function of the scale factor interpolated from tabulated values
    
    - a_of_t : Returns the scale factor as a function of cosmic time in Myr
    
    - a_of_t_fast : Returns the scale factor as a function of cosmic time in Myr interpolated from tabulated values
    
    - tau_dyn : Returns the dynamical time at a given scale factor
    
    - tau_dyn_of_t : Returns the dynamical time at a given cosmic time in Myr
    
    - T : Returns cosmic time normalised to dynamical time between two scale factors
    '''
    def __init__(self, H0 = 67, Om0 = 0.3, OL0 = 0.7):
        self._H0 = H0
        self._Om0 = Om0
        self._OL0 = OL0
        self._Ok0 = 1 - Om0 - OL0
        
        self.__t_arr = np.logspace(-3,5, 300)
        self.__a_arr = np.vectorize(self.a_of_t)(self.__t_arr)

        self.a_of_t_fast = interp1d(self.__t_arr, self.__a_arr, kind='cubic')
        self.t_of_a_fast = interp1d(self.__a_arr, self.__t_arr, kind="cubic")
        
    def get_cosmo_params(self):
        """
        Getter function for cosmological parameters
        
        Returns:
        ----------
        H0: (float) Hubble constant at z = 0
        Om0: (float) Matter density parameter at z = 0
        OL0: (float) Dark Energy density parameter at z = 0
        """
        return self._H0, self._Om0, self._OL0
    
    def set_cosmo_params(self, H0 = None, Om0 = None, OL0 = None):
        '''
        Setter function for cosmological parameters. 
        Allows to update or or several cosmological parameters

        Parameters:
        ----------
        H0: (float) Hubble constant at z = 0
        Om0: (float) Matter density parameter at z = 0
        OL0: (float) Dark Energy density parameter at z = 0

        '''
        if H0 is not None:
            self._H0 = H0
        if Om0 is not None:
            self._Om0 = Om0
        if OL0 is not None:
            self._OL0 = OL0
        self._Ok0 = 1 - Om0 - OL0
        return None
    
    def O(self, a):
        '''
        Matter density parameter evalutated at scale factor a

        Parameters:
        ----------
        a: (float) scale factor

        Returns:
        ----------
        Om: (float) The matter density parameter at scale factor a
        '''
        return self._Om0 * a**-3 /(self._OL0 + self._Ok0*a**-2 + self._Om0*a**-3)

    def Hubble_a(self, a):
        '''
        Hubble parameter evalutated at scale factor a

        Parameters:
        ----------
        a: (float) scale factor

        Returns:
        ----------
        H: (float) The Hubble parameter at scale factor a in units of Myr^{-1}
        '''
        return self._H0/100 * 1/9.77792e3 * np.sqrt( self._Om0*a**(-3) + self._Ok0*a**(-2) + self._OL0)

    def t_of_a(self, a):
        '''
        Cosmic time t evalutated at scale factor a

        Parameters:
        ----------
        a: (float) scale factor

        Returns:
        ----------
        t: (float) Cosmic time at scale factor a in Myr
        '''
        a = np.array(a)
        res = np.zeros_like(a)
        if a.size == 1:
            t,err = quad(lambda ap : 1.0/(ap*self.Hubble_a(ap)),0,a) 
            res = t
        else: 
            for i,ai in enumerate(a):
                t,err = quad(lambda ap : 1.0/(ap*self.Hubble_a(ap)),0,ai) 
                res[i] = t
        return res

    def a_of_t(self, t):
        '''
        Scale factor a evalutated at cosmic time t

        Parameters:
        ----------
        t: (float) Cosmic time at scale factor a in Myr

        Returns:
        ----------
        a: (float) scale factor
        '''
        f = lambda x: self.t_of_a(x) - t
        a = root(f, 0.5)['x'][0]
        return a

    def tau_dyn(self, a):
        '''
        Dark matter halo dynamical time (Jiang & van den Bosch 2016)

        Parameters:
        ----------
        a: (float) scale factor

        Returns:
        ----------
        tau: (float) dynamical time evaluated at scale factor a
        '''
        x = (self.O(a) - 1)
        Delta_vir = (18*np.pi*np.pi + 82*x - 39*x**2)
        return 1/self.Hubble_a(a)*np.sqrt(np.pi**2/2/Delta_vir)

    def tau_dyn_of_t(self, t):
        '''
        Dark matter halo dynamical time (Jiang & van den Bosch 2016)

        Parameters:
        ----------
        t: (float) Cosmic time in Myr

        Returns:
        ----------
        tau: (float) dynamical time evaluated at cosmic time t
        '''
        a = self.a_of_t_fast(t)
        x = (self.O(a) - 1)
        Delta_vir = (18*np.pi*np.pi + 82*x - 39*x**2)
        return 1/self.Hubble_a(a)*np.sqrt(np.pi**2/2/Delta_vir) 
        
    def T(self, a, a_ref):
        '''
        Cosmic time in units of the dynamical time (Wang et al. 2020)

        Parameters:
        ----------
        a: (float) scale factor
        a_ref: (float) reference scale factor, sets zero point 

        Returns:
        ----------
        T: (float) Cosmic time in units of the dynamical time 
        '''
        dT = lambda t: 1/self.tau_dyn_of_t(t)
        res, err = quad(dT, self.t_of_a_fast(a_ref), self.t_of_a_fast(a))
        return res

def burr12(x, a, b, scale):
    '''
    Burr type XII distribtution
    $$f(x,a,b,\lambda,\sigma) = \frac{ab}{\sigma}\left(\frac{x-\lambda}{\sigma}\right)^{a-1}\left[1+\left(\frac{x-\lambda}{\sigma}\right)^2\right]^{-b-1}$$
    here $\lambda$ is fixed to 0
    Parameters:
    ----------
    x: (float, array of floats) points at which to evatuate the distribution
    a: (float) 
    b: (float) 
    scale: (float) sigma
    Returns:
    ----------
    f: (float) 
    '''
    return st.burr12.pdf(x,a,b, loc = 0, scale = scale)

def exp_arg(x,y,mu,cov):
    '''
    Explicit decomposition of the argument of a 2D Normal distribution for vectorisation purposes,
    this function is to be called for a single position but multiple parameter vectors
    
    Parameters:
    ----------
    x: (float) point at which to evatuate the argument
    y: (float) point at which to evatuate the argument
    mu: ((2,n) floats) mean vectors
    cov: ((2,2,n) floats) covariance matrices 
    
    Returns:
    ----------
    arg: ((n,) floats)
    '''
    #D = np.linalg.inv(cov)
    D = np.zeros(cov.shape)
    detC = cov[0,0,:]*cov[1,1,:] - cov[0,1,:]*cov[1,0,:]
    D[0,0,:] = cov[1,1,:]/detC[:]
    D[0,1,:] = -cov[0,1,:]/detC[:]
    D[1,0,:] = -cov[1,0,:]/detC[:]
    D[1,1,:] = cov[0,0,:]/detC[:]
    y0 = x - mu[0,:]
    y1 = y - mu[1,:]
    return y0*D[0,0,:]*y0 + y1*D[1,0,:]*y0 + y0*D[0,1,:]*y1 + y1*D[1,1,:]*y1

def calculate_3D_likelihood(r,tet,phi, param, mu, cov):
    '''
    Calculates 3 sparsity likelihood at a single set of spherical coordiantes for multiple parameter vectors
    
    Parameters:
    ----------
    r: (float) radial sparsity coordinate
    tet: (float) azimuthal sparsity coordinate
    phi: (float) polar sparsity angle
    param ((3,n) floats) Burr XII parameter vectors
    mu: ((2,n) floats) mean vectors
    cov: ((2,2,n) floats) covariance matrices 
    
    Returns:
    ----------
    f: ((n,) floats)
    '''
    detC = cov[0,0,:]*cov[1,1,:] - cov[0,1,:]*cov[1,0,:]
    return 1/2/np.pi/np.sqrt(np.abs(detC)) * burr12(r,*param) * np.exp(-0.5 * exp_arg(tet, phi, mu, cov))


def get_3D_likelihood_params(j, lastMM, pos):
    '''
    function that estimates the 3 sparsity parameter vector for a last merger at snapshot j
    
    Parameters:
    ----------
    j: (int) snapshot ID at which to estimate the vector
    lastMM: (array of floats) ROCKSTAR last Major Merger data 
    pos: (3,floats) polar sparsity coordinates

    Returns:
    ----------
    param ((3,) floats) Burr XII parameter vectors
    mu: ((2,) floats) mean vectors
    cov: ((2,2) floats) covariance matrices 
    '''
    r, tet, phi = pos
    bounds = (0, 25)
    study = (lastMM == np.unique(lastMM)[j])
    r_bins = np.logspace(np.log10(r[np.isfinite(r)].min()), np.log10(r[np.isfinite(r)].max()), int(np.sqrt(r.size)))
    rho_r, _ = np.histogram(r[study], r_bins, density = True)
    mbins = 0.5*(r_bins[1:] + r_bins[:-1])
    try:
        param, C = curve_fit(burr12, mbins[rho_r>1e-2], rho_r[rho_r>1e-2], bounds = bounds, **{'maxfev':2000})
    except RuntimeError:
        param = [0,0,0]
 
    cond = np.isfinite(tet) & np.isfinite(phi) & study
    XX = np.vstack([tet[cond],phi[cond]])
    mu_vec = np.mean(XX, axis = 1)
    cov = np.cov(XX, rowvar = 1)
    return param, mu_vec, cov

def gen_beta_prime(x,a,b,p,q):
    '''
    Generalised beta prime distribution
    $$\rho(x,\alpha,\beta,p,q) = \frac{p\left(\frac{x}{q}\right)^{\alpha p - 1}\left(1+\left(\frac{x}{q}\right)^p\right)^{-\alpha-\beta}}{q\,B(\alpha,\beta)}$$
    here $\lambda$ is fixed to 0
    Parameters:
    ----------
    x: (float, array of floats) points at which to evatuate the distribution
    a: (float) alpha
    b: (float) beta
    p: (float)
    q: (float)
    Returns:
    ----------
    rho: (float, array of floats) 
    '''
    return (p*(x/q)**(a*p - 1) * (1 + (x/q)**p)**-(a+b))/q/beta(a,b)

def log_normal(x, mu, sig):
    '''
    Log-Normal distribution
    $$\frac{1}{x\sigma\sqrt{2\pi}}\exp\left[-\frac{1}{2}\frac{\left(\log(x) - mu\right)^2}{\sigma^2}\right]$$
    Parameters:
    ----------
    x: (float, array of floats) points at which to evatuate the distribution
    mu: (float) mu
    sig: (float) sigma
    Returns:
    ----------
    rho: (float, array of floats) 
    '''
    return 1/(x*sig*np.sqrt(2*np.pi))*np.exp(-0.5*(np.log(x) - mu)**2/sig**2)

def get_1D_likelihood_params(lastMM, s):
    '''
    function that estimates the 1 sparsity parameter vector for all previous times of merger
    
    Parameters:
    ----------
    lastMM: (array of floats) ROCKSTAR last Major Merger data 
    pos: (3,floats) polar sparsity coordinates

    Returns:
    ----------
    p ((4,) floats) generalised beta prime parameter vectors
    '''
    nbins = 50
    s_bins = np.linspace(1,10,nbins)
    MM_bins = np.unique(lastMM)#np.linspace(0,1,50)
    bounds = (0, 25)
    #H, x_edges, y_edges = np.histogram2d(s, lastMM, bins= (s_bins, MM_bins))
    #m_edges = .5*(x_edges[1:] + x_edges[:-1])

    p = np.zeros((len(MM_bins),4))
    err_p = np.zeros((len(MM_bins),4))
    X2 = np.zeros(len(MM_bins))

    for i in range(len(MM_bins)):
        #xdata = m_edges - 1
        study = (lastMM == MM_bins[i])
        x = s[study]-1
        x_bins = np.logspace(np.log10(np.nanmin(x)), np.log10(np.nanmax(x)), 200)# int(np.sqrt(x.size)))
        ydata, bins = np.histogram(x, x_bins, density = True)
        xdata = .5*(bins[1:] + bins[:-1])
        #ydata = #H[:,i]/simps(H[:,i], m_edges-1)
        try:
            cond = ydata > ydata[ydata!=0].min() * 10
            p[i,:], C = curve_fit(gen_beta_prime, xdata[cond], ydata[cond], p0 = (1,1,1,1), bounds = bounds, **{"maxfev":4000})
            err_p[i,:] = np.sqrt(np.diag(C))
            X2[i] = np.sum((ydata - gen_beta_prime(xdata,*p[i,:]))**2)/(len(xdata[cond]) - 4)
        except RuntimeError:
            p[i,:] = (np.nan,)*4
    return p
      
    
def import_parameter_grid(file):
    '''
    function that import premade parameter vector grids ready for interpolation
    
    Parameters:
    ----------
    file: (string) location of file containing parameter vector grid

    Returns:
    ----------
    points ((n,2) floats) points at which the parameter grids are evaluated
    tet_vals1D ((n,4) floats) values of the 1 sparsity parameter vectors at the points
    tet_vals1D ((n,9) floats) values of the 3 sparsity parameter vectors at the points
    '''
    with h5py.File(file, "r") as f:
        points = f['points'][:]
        tet_vals3D = f['3D_params'][:] 
        tet_vals1D = f['1D_params'][:]
        a_pval = f['a_pval'][:]
        tet_pval = f['pval_params'][:]
    return points, tet_vals1D, tet_vals3D, a_pval, tet_pval

def get_params_z(z_obs, points, tet_vals, clock, npts = 50):
    '''
    function that interpolates the parameter vector to the redshift of the object
    
    Parameters:
    ----------
    z_obs: (float) objects redshift
    points: ((n,2) floats) points at which the parameter grids are evaluated
    tet_vals: ((n,m) floats) values of sparsity parameter vectors at the points
    clock: (CosmoClock) cosmology clock
    npts: (int) number of points at which to interpolate the parameter vectors
    
    Returns:
    ----------
    T_grid ((npts) floats) points at which the parameter vector grids have been evaluated as dynamical times before the time of observation
    tet_grids ((npts, m) floats) values of the sparsity parameter vectors at the interpolated points
    a_grid ((npts) floats) points at which the parameter vector grids have been evaluated as scale factors
    '''
    a_obs = 1/(z_obs + 1)
    a_grid = np.logspace(np.log10(0.08),np.log10(a_obs), npts)
    T_grid = np.vectorize(clock.T)(a_grid, a_obs)
    z_grid = np.array([z_obs,])
    tet_grids = np.array([griddata(points, tet_vals[l],(z_grid, T_grid), method='linear') for l in range(len(tet_vals))])
    return T_grid, tet_grids, a_grid

def get_median_dichotomy(x, P):
    '''
    function that finds the point, x_i, closest to the median of distribution P sampled at positions x using a dichotomy search procedure.
    
    Parameters:
    ----------
    x: ((n,) floats) points at which the distribution is sampled
    P: ((n,) floats) values of the distribution at the points
    
    Returns:
    ----------
    x_med (float) median point from samples or if this point falls between two points the mean between the two closest points.
    '''
    norm = simps(P,x)
    P = P/norm
    mu = simps(x*P,x)
    imed = np.argmin(np.abs(x-mu))
    ibot = 0
    itop = imed#len(x)//2
    while np.abs(simps(P[:itop], x[:itop]) - 0.5) > 0.01:
        #print(f"{simps(P[:itop], x[:itop]):.3} [{ibot}:{itop}]")
        if np.abs(itop - ibot) <= 1:
            return .5*(x[itop] + x[ibot])
        if simps(P[:itop], x[:itop]) > 0.5:
            inew = itop - (itop - ibot)//2
            if simps(P[:inew], x[:inew]) < 0.5:
                #itop = imed
                ibot = inew
                continue
            #ibot = itop
            itop = inew
            continue
        else:
            inew = itop + (itop - ibot)//2
            if simps(P[:inew], x[:inew]) < 0.5:
                #itop = imed
                ibot = inew
                continue
            ibot = itop
            itop = inew
            continue
    return x[itop]

def get_median(x,P):
    '''
    function that finds the value, x_med, coresponding to the mdeian of P using root finding.
    
    Parameters:
    ----------
    x: ((n,) floats) points at which the distribution is sampled
    P: ((n,) floats) values of the distribution at the points
    
    Returns:
    ----------
    x_med (float) median point from samples or if this point falls between two points the mean between the two closest points.
    '''
    norm = simps(P,x)
    P = P/norm
    p = interp1d(x, P, kind = 'quadratic', bounds_error = False, fill_value = 0)

    res_root = root(np.vectorize(lambda x: simps(p(np.linspace(0,x,500)),np.linspace(0,x,500))-.5), x0 = 0.5 + 0.1*np.random.randn())
    x_med = res_root['x'][0]
    count = 0
    while x_med > 1 or x_med < 0 or not res_root['success']:
        count += 1
        res_root = root(np.vectorize(lambda x: simps(p(np.linspace(0,x,500)),np.linspace(0,x,500))-.5), x0 = 0.5 + 0.1*np.random.randn())
        x_med = res_root['x'][0]
        if count > 100:
            print('Median finder timed out... reverting to dichotomy', flush = True)
            x_med = get_median_dichotomy(x,P)
            break
    return x_med

def credible_interval(x, P, conf = 0.68, verbose = False):
    '''
    function that calculates the credible interval of a distrbituion P. Here the credible interval is defined as the highest density interval.
    
    Parameters:
    ----------
    x: ((n,) floats) points at which the distribution is sampled
    P: ((n,) floats) values of the distribution at the points
    conf: (float) Probability to be contained in the confidence interval.
    
    Returns:
    ----------
    x_low (float) lower bound of the confidence interval
    x_med (float) median point of the distribution
    x_high (float) upper bound of the confidence interval
    '''
    
    norm = simps(P,x)
    P = P/norm
    x_med = get_median(x, P)
    p = interp1d(x, P, kind = 'cubic', bounds_error = False, fill_value = 0)
    y_med = p(x_med)

    def prob_at_level(a):
        cond = P > y_med - a
        try:
            x_start_low = x[cond].min()
            x_low = root(lambda z: p(z) - y_med + a, x0 = x_start_low)['x'][0]
            #print(x_low)
            x_start_high = x[cond].max()
            x_high = root(lambda z: p(z) - y_med + a, x0 = x_start_high)['x'][0]
        except ValueError:
            return 0
        #print(x_high)
        x_int = np.linspace(x_low, x_high, 500)
        return simps(p(x_int), x_int)
    root_res = root(lambda a: prob_at_level(a) - conf, x0 = 0)
    a_res = root_res['x'][0]
    cond = P > y_med - a_res

    if not root_res['success']:
        if verbose:
            print(f"Initial attempt failed to converge, trying brute force.", flush = True)
        x_low, x_max, x_high = credible_interval_iterative(x, P, conf = conf)
        ilow = np.where(x == x_low)[0][0]
        ihigh = np.where(x == x_high)[0][0]
        def two_point_prob(x):
            x_int = np.linspace(x[0], x[1], 500)
            return simps(p(x_int), x_int) - conf, p(x[0]) - p(x[1])
        root_res = root(two_point_prob, x0 = (x_low, x_high))
        
        if root_res['success']:
            x_low, x_high = root_res['x']
            x_int = np.linspace(x_low, x_high, 500)
            if np.abs(simps(p(x_int), x_int) - conf) > 1e-5:
                if verbose:
                    print(f"Brute force also failed to converge, returning symetric credible interval.", flush = True)
                def symmetric_interval(c):
                    x_int = np.linspace(x_med-c/2, x_med+c/2, 500)
                    return simps(p(x_int), x_int) - conf
                root_res = root(np.vectorize(symmetric_interval), x0 = 0.02)
                if root_res['success']:
                    c = root_res['x'] 
                    x_low, x_high = (x_med + 0.5*np.array([-c,c])).reshape(-1)
        else:
            if verbose:
                print(f"Brute force also failed to converge, returning symetric credible interval.", flush = True)
            def symmetric_interval(c):
                x_int = np.linspace(x_med-c/2, x_med+c/2, 500)
                return simps(p(x_int), x_int) - conf
            root_res = root(np.vectorize(symmetric_interval), x0 = 0.02)
            if root_res['success']:
                c = root_res['x'] 
                x_low, x_high = (x_med + 0.5*np.array([-c,c])).reshape(-1)
    else:
        x_low = root(lambda z: p(z) - y_med + a_res, x0 = x[cond].min())['x'][0]
        x_high = root(lambda z: p(z) - y_med + a_res, x0 =  x[cond].max())['x'][0]
    if verbose:
        x_int = np.linspace(x_low, x_high, 500)
        print(f"Found interval, coresponding probability is {simps(p(x_int), x_int)}", flush = True)
    if x_high >= x.max():
        x_high = x.max()
    if x_low <= x.min():
        x_low = x.min()
    return x_low, x_med, x_high

def credible_interval_iterative(x, P, conf = 0.68):
    '''
    function that calculates the credible interval of a distrbituion P sampled at points x using an iterative "spreading" procedure.
    
    Parameters:
    ----------
    x: ((n,) floats) points at which the distribution is sampled
    P: ((n,) floats) values of the distribution at the points
    conf: (float) Probability to be contained in the confidence interval.
    
    Returns:
    ----------
    x_low (float) lower bound of the confidence interval
    x_med (float) median point of the distribution
    x_high (float) upper bound of the confidence interval
    '''
    norm = simps(P,x)
    P = P/norm
    x_med = get_median(x, P)
    #imax = np.argmax(P)
    if np.where(x == x_med)[0].size > 0:
        imax = np.where(x == x_med)[0][0]
    else:
        #y_med = interp1d(x, P)(x_med)
        imax = np.argmin(np.abs(x-x_med))
    ilow = imax-1
    ihigh = imax+1
    while simps(P[ilow:ihigh], x[ilow:ihigh]) < conf:
        #print(simps(P[ilow:ihigh], x[ilow:ihigh]), end = ' ')
        if ilow > 0 and ihigh < len(P)-1:
            if P[ilow - 1] < P[ihigh + 1]:
                ihigh += 1
            else:
                ilow += -1
        elif ilow <= 0:
            ihigh += 1
            ilow = 0
        elif ihigh >= len(P) - 1:
            ilow -=1
            ihigh = len(P) - 1
    try:
        return x[ilow], x[imax], x[ihigh]
    except IndexError:
        if ilow < 0:
            ilow = 0
        if ihigh > len(P) - 1:
            ihigh = len(P) - 1
        return x[ilow], x[imax], x[ihigh]
    
    
def bf(x, P, x_cut = 0.7):
    '''
    function that computes the Bayes factor coresponding to a distribution P sampled at points x and for a hypothesis cut at x_cut
    
    Parameters:
    ----------
    x: ((n,) floats) points at which the posterior distribution is sampled
    P: ((n,) floats) values of the posterior distribution at the points
    x_cut: (float) value of x delimiting the two hypotheses
    
    Returns:
    ----------
    BFac (float) Bayes factor corresponding to this posterior and hypotheses 
    '''
    
    cond = x > x_cut
    P1 = P[cond]
    x1 = x[cond]
    P0 = P[~cond]
    x0 = x[~cond]
    BFac = simps(P1, x1)/simps(P0, x0)
    return BFac

def p_value(s, a, b, p, q):
    '''
    function that computes the p-value according to 1 sparsity test without uncertainty.
    
    Parameters:
    ----------
    s: (float) sparsity, s_{200,500}
    a: (float) 1 sparsity likelihood parameter alpha
    b: (float) 1 sparsity likelihood parameter beta
    p: (float) 1 sparsity likelihood parameter p
    q: (float) 1 sparsity likelihood parameter q
    
    Returns:
    ----------
    p_val (float) p-value for the corresponding sparsity
    '''
    p_val = 1-quad(lambda x: gen_beta_prime(x, a,b,p,q),1e-4,s - 1)[0]
    return p_val

def p_value_uncertainty(s, ds, a, b, p, q):
    '''
    function that computes the p-value according to 1 sparsity test with uncertainty.
    
    Parameters:
    ----------
    s: (float) sparsity, s_{200,500}
    ds: (float) sparsity error, ds_{200,500}
    a: (float) 1 sparsity likelihood parameter alpha
    b: (float) 1 sparsity likelihood parameter beta
    p: (float) 1 sparsity likelihood parameter p
    q: (float) 1 sparsity likelihood parameter q
    
    Returns:
    ----------
    p_val (float) p_value for the corresponding sparsity accounting for uncertainty
    '''
    mu = np.log(s-1)
    sig = np.sqrt(np.log(.5*(np.sqrt(4*ds**2/(s-1)**2 + 1) + 1)))
    p_val = 1-dblquad(lambda x, s: log_normal(s-1, mu, sig)*gen_beta_prime(x, a,b,p,q), 1, 50, lambda s: 0, lambda s: s - 1)[0]
    return p_val

def s_of_pvalue(p_val, a , b, p, q):
    '''
    function that computes the sparsity corresponding to a p-value.
    
    Parameters:
    ----------
    p_val: (float) p-value for the s_{200,500} test
    a: (float) 1 sparsity likelihood parameter alpha
    b: (float) 1 sparsity likelihood parameter beta
    p: (float) 1 sparsity likelihood parameter p
    q: (float) 1 sparsity likelihood parameter q
    
    Returns:
    ----------
    s (float) sparsity corresponding to the input p-value
    '''
    s = root(lambda x: p_value(x, a, b, p, q) - p_val, x0 = 1.5)['x'][0]
    return s

def effective_sparsity(s, ds, a, b, p, q):
    '''
    function that computes the effective sparsity that equtes to the sqme p-value for the sparsity test with uncertainty.
    
    Parameters:
    ----------
    s: (float) sparsity, s_{200,500}
    ds: (float) sparsity error, ds_{200,500}
    a: (float) 1 sparsity likelihood parameter alpha
    b: (float) 1 sparsity likelihood parameter beta
    p: (float) 1 sparsity likelihood parameter p
    q: (float) 1 sparsity likelihood parameter q
    
    Returns:
    ----------
    s_eff (float) effectibe sparsity accounting for uncertainty
    '''
    p_val = p_value_uncertainty(s, ds, a, b, p, q)
    s_eff = s_of_pvalue(p_val, a, b, p, q)
    return s_eff
    
class MergerTimer:
    def __init__(self, M200 = None, M500 = None, M2500 = None, dM200 = None, dM500 = None, dM2500 = None, z_obs = 0, clock = None, prior = 'flat-a', verbose = False):
        '''
        MergerTimer class. Used to estimate the epoch of the last ajor merger of a dark matter halo

        Parameters:
        ----------
        M200: (float) Observed mass at an overdensity of 200 (Required)
        M500: (float) Observed mass at an overdensity of 500 (Required)
        M2500: (float) Observed mass at an overdensity of 2500 (Optional)
        z_obs: (float) Observed redshift of the object
        clock: (CosmoClock) CosmoClock coresponding to the cosmological model to be used
        prior: (str) prior type to use, current options include 'flat-a', 'flat-t' and 'flat-z' defaults to 'flat-a'
        verbose: (bool) toogles verbosity of the calculations
        
        Methods:
        ----------
        - interp_params_1D : Interpolates the 1 sparsity likelihood parameters for a given value of a, the scale factor
        
        - interp_params_3D : Interpolates the 3 sparsity likelihood parameters for a given value of a, the scale factor
        
        - loglike_1D : Calculates the 1 sparsity likelihood at scale factor a, incorporating the parameters interpolated at the scale factor of observation
        
        - loglike_3D : Calculates the 3 sparsity likelihood at scale factor a, incorporating the parameters interpolated at the scale factor of observation
        
        - run : Runs the MergerTimer to produce analysed data
        
        - get_chains : Returns MCMC chains in numpy arrays of shape (nsteps, nwalkers, 1)
        
        - plot_posteriors : Quick plotting utility to visualise results
        
        - latexise_results : Quick formating utility to output results in a LaTeX freindly format
        
        - Gelman_Rubin : Gelman-Rubin Convergence criterion to verify MCMC is well convergenged
        '''
        self._verbose = verbose
        assert type(clock) == CosmoClock, "The timer requires a CosmoClock to function"
        assert prior in ('flat-a','flat-z','flat-t'), f"Unknown prior '{prior}', currently suported options are 'flat-a' , 'flat-z' and 'flat-t'"
        if self._verbose:
            print("Initialising Timer", flush = True)
        self._M200 = M200
        self._M500 = M500
        self._M2500 = M2500
        self._dM200 = dM200
        self._dM500 = dM500
        self._dM2500 = dM2500
        self._z_obs = z_obs
        self._a_obs = 1/(1+z_obs)
        self._clock = clock
        self._prior = prior
        if M200 is None or M500 is None:
            if M200 is None and M500 is not None:
                raise ValueError("Please specify at least one value for M200")
            if M200 is not None and M500 is None:
                raise ValueError("Please specify at least one value for M500")
            if M200 is None and M500 is None:
                raise ValueError("Please specify at least one value for M200 and M500")
                
        self._s200_500 = M200/M500
        
        self.__uncertainties_1D = False
        if None not in (dM200, dM500):
            self.__uncertainties_1D = True
            self._ds200_500 = self._s200_500 * np.sqrt((dM200/M200)**2 + (dM500/M500)**2)
        #Setting up grid points and interpolation functions
        if self._verbose:
            print(f"Importing parmater grids and setting up parameter vectors at z = {self._z_obs}", flush = True)
        self.__points, self.__tet_vals1D, self.__tet_vals3D, self.__a_pval, self.__tet_pval = import_parameter_grid(file = package_directory+"/parameter_vectors/Uchuu_parameter_vectors.hdf5")
        
        self._T_grid, self._tet1D_grids, self._a_grid = get_params_z(self._z_obs, self.__points, self.__tet_vals1D, clock = clock, npts = 100)
        self.interp_params1D = interp1d(self._a_grid, self._tet1D_grids, kind = "linear", fill_value = 1e-15, bounds_error = False)
        
        self._T_grid, self._tet3D_grids, self._a_grid = get_params_z(self._z_obs, self.__points, self.__tet_vals3D, clock = clock, npts = 100)
        self.interp_params3D = interp1d(self._a_grid, self._tet3D_grids, kind = "linear", fill_value = 1e-15, bounds_error = False)
        
        self._pval_params = np.array([interp1d(self.__a_pval, self.__tet_pval[:,i], kind = "linear", fill_value = 1e-15, bounds_error = False)(self._a_obs) for i in range(4)])
        
        if M2500 is not None:
            if self._verbose:
                print("M2500 specified allowing 3 sparsity estimation!", flush = True)
            self.__measure_3D = True
            self._s = np.array([M200/M500, M200/M2500, M500/M2500])
            #ds = s*np.sqrt(np.array([(dM200/M200)**2 + (dM500/M500)**2,(dM200/M200)**2 + (dM2500/M2500)**2, (dM500/M500)**2 + (dM2500/M2500)**2]))

            #calculate 3D position
            x_temp = self._s-1
            self._pos = np.array([np.sqrt(np.sum(x_temp**2, axis = 0)), np.arctan(np.sqrt(np.sum(x_temp[:2]**2, axis = 0))/x_temp[2]), np.arctan(x_temp[1]/x_temp[0])])
            self.__uncertainties_3D = False
            if None not in (dM200, dM500, dM2500):
                self.__uncertainties_3D = True
                self._ds = self._s *np.sqrt(np.array([(dM200/M200)**2 + (dM500/M500)**2,(dM200/M200)**2 + (dM2500/M2500)**2, (dM500/M500)**2 + (dM2500/M2500)**2]))
        else:
            if self._verbose:
                print("Only M200 and M500 specified, sticking to 1 sparsity estimation!", flush = True)
            self.__measure_3D = False
        self.res = {}
        if self._verbose:
            print("Timer is ready, use MergerTimer.run() to run the timer", flush = True)
        self.__runned = False
        self._samples1D = np.array([])
        self._samples3D = np.array([])
        return None
        
    def set_masses(self, M200 = None, M500 = None, M2500 = None, dM200 = None, dM500 = None, dM2500 = None):
        self._M200 = M200
        self._M500 = M500
        self._M2500 = M2500
        self._dM200 = dM200
        self._dM500 = dM500
        self._dM2500 = dM2500
        self._s200_500 = M200/M500
        
        self.__uncertainties_1D = False
        if None not in (dM200, dM500):
            self.__uncertainties_1D = True
            self._ds200_500 = self._s200_500 * np.sqrt((dM200/M200)**2 + (dM500/M500)**2)
        
        if M2500 is not None:
            if self._verbose:
                print("M2500 specified allowing 3 sparsity estimation!", flush = True)
            self.__measure_3D = True
            self._s = np.array([M200/M500, M200/M2500, M500/M2500])

            #calculate 3D position
            x_temp = self._s-1
            self._pos = np.array([np.sqrt(np.sum(x_temp**2, axis = 0)), np.arctan(np.sqrt(np.sum(x_temp[:2]**2, axis = 0))/x_temp[2]), np.arctan(x_temp[1]/x_temp[0])])
            self.__uncertainties_3D = False
            if None not in (dM200, dM500, dM2500):
                self.__uncertainties_3D = True
                self._ds = self._s *np.sqrt(np.array([(dM200/M200)**2 + (dM500/M500)**2,(dM200/M200)**2 + (dM2500/M2500)**2, (dM500/M500)**2 + (dM2500/M2500)**2]))
        else:
            if self._verbose:
                print("Only M200 and M500 specified, sticking to 1 sparsity estimation!", flush = True)
            self.__measure_3D = False
        return None
            
    def set_redshift(self, z_obs):
        self._z_obs = z_obs
        self._a_obs = 1/(1+z_obs)
        if self._verbose:
            print(f"Importing parmater grids and setting up parameter vectors at z = {self._z_obs}", flush = True)
            
        self.__points, self.__tet_vals1D, self.__tet_vals3D, self.__a_pval, self.__tet_pval = import_parameter_grid(file = package_directory+"/parameter_vectors/Uchuu_parameter_vectors.hdf5")
        
        self._T_grid, self._tet1D_grids, self._a_grid = get_params_z(self._z_obs, self.__points, self.__tet_vals1D, clock = self._clock, npts = 100)
        self.interp_params1D = interp1d(self._a_grid, self._tet1D_grids, kind = "linear", fill_value = 1e-15, bounds_error = False)
        
        self._T_grid, self._tet3D_grids, self._a_grid = get_params_z(self._z_obs, self.__points, self.__tet_vals3D, clock = self._clock, npts = 100)
        self.interp_params3D = interp1d(self._a_grid, self._tet3D_grids, kind = "linear", fill_value = 1e-15, bounds_error = False)
        
        self._pval_params = np.array([interp1d(self.__a_pval, self.__tet_pval[:,i], kind = "linear", fill_value = 1e-15, bounds_error = False)(self._a_obs) for i in range(4)])
        return None
        
    def loglike3D(self, a):
        '''
        function that computes the 3 sparsity likelihood at for a last major merger at scale factor(s) a

        Parameters:
        ----------
        a: ((n,) floats) scale factors at which to calculate the likelihood function

        Returns:
        ----------
        L: ((n,) floats) likelihood function sampled at a
        '''
        pos = self._pos
        orig_shape = a.shape
        a = a.flatten()
        L = np.zeros(a.shape)
        cond = (a>0.1) & (a<self._a_obs)
        param_vec = self.interp_params3D(a[cond])
        pp = param_vec[:3,:]
        mumu = param_vec[4:6,:]
        covcov = param_vec[6:,:]
        COVCOV = np.zeros((2,2,a[cond].size))
        COVCOV[0,0,:] = covcov[0,:]; COVCOV[1,1,:] = covcov[2,:]; COVCOV[0,1,:] = covcov[1,:]; COVCOV[1,0,:] = covcov[1,:]
        L[cond] = np.log(calculate_3D_likelihood(pos[0], pos[1], pos[2], pp, mumu, COVCOV))
        L[~cond] = -np.inf
        prior = np.zeros(a.shape)
        if self._prior == 'flat-a':
            prior[cond] = 1
        elif self._prior == 'flat-z':
            prior[cond] = 1/a[cond]/a[cond] * 1/(9 - self._z_obs)
        elif self._prior == 'flat-t':
            t0 = self._clock.t_of_a_fast(1/(1+self._z_obs))
            tmin = self._clock.t_of_a_fast(0.1)
            prior[cond] = 1/(t0 - tmin) * 1/a[cond]/self._clock.Hubble_a(a[cond])
        else:
            raise ValueError(f"Unknown prior '{self._prior}', currently suported options are 'flat-a' , 'flat-z' and 'flat-t'")
        prior[~cond] = np.inf
        L = L * prior
        return L.reshape(orig_shape)
    
    def loglike1D(self, a):
        '''
        function that computes the 1 sparsity likelihood at for a last major merger at scale factor(s) a

        Parameters:
        ----------
        a: ((n,) floats) scale factors at which to calculate the likelihood function

        Returns:
        ----------
        L: ((n,) floats) likelihood function sampled at a
        '''
        a = np.array(a)
        orig_shape = a.shape
        cond = (a>0.1) & (a<self._a_obs)
        L = np.zeros(a.shape)
        param_vec = self.interp_params1D(a[cond])
        L[cond] = np.log(gen_beta_prime(self._s200_500-1, *param_vec))
        prior = np.ones(a.size)
        L[~cond] = -np.inf
        prior = np.zeros(a.shape)
        if self._prior == 'flat-a':
            prior[cond] = 1
        elif self._prior == 'flat-z':
            prior[cond] = 1/a[cond]/a[cond] * 1/(9 - self._z_obs)
        elif self._prior == 'flat-t':
            t0 = self._clock.t_of_a_fast(1/(1+self._z_obs))
            tmin = self._clock.t_of_a_fast(0.1)
            prior[cond] = 1/(t0 - tmin) * 1/a[cond]/self._clock.Hubble_a(a[cond])
        else:
            raise ValueError(f"Unknown prior '{self._prior}', currently suported options are 'flat-a' , 'flat-z' and 'flat-t'")
        prior[~cond] = np.inf
        L = L * prior
        return L.reshape(orig_shape)
    
    def run(self, nwalkers = 10, nsteps = 10000):
        '''
        Initiates MCMC sampling of the posterior distribtuion function(s)  

        Parameters:
        ----------
        nwalker: (int) number of random walkers for the MCMC chains
        nsteps: (int) number of steps made by each walker

        Returns:
        ----------
        res: (dict) ditionary containing the estimated posterior likelyhood function(s) and sample points.
        Once calculated this distionarry can be accessed as an atribute of the MergerTimer
        '''
        if self._verbose:
            print("Calculating analytical p-value...", end = ' ', flush = True)
        p_val = p_value(self._s200_500, *self._pval_params)
        self.res['pval'] = p_val
        
        if self.__uncertainties_1D:
            if self._verbose:
                print("Incorporating errors on masses...", end = ' ', flush = True)
            p_val_unc = p_value_uncertainty(self._s200_500, self._ds200_500, *self._pval_params)
            self.res['pval_unc'] = p_val_unc
        
        if self._verbose:
            print("Done!", flush = True)
            print("Sampling 1 sparsity posterior...", end = ' ', flush = True)
        ndim = 1
        p0 = np.random.randn(nwalkers, ndim)*0.1+0.5
        sampler1D = emcee.EnsembleSampler(nwalkers, ndim, self.loglike1D, vectorize = True)
        state = sampler1D.run_mcmc(p0, 100)
        sampler1D.reset()
        run1D = sampler1D.run_mcmc(state,nsteps)
        self._samples1D = sampler1D.get_chain(flat = False)
        
        if self._verbose:
            print("Done!", flush = True)
            
        x_cut = root(lambda x: self._clock.T(x,self._a_obs) + 2, x0 = 0.5)['x'][0]
        post1D, bins = np.histogram(np.hstack(self._samples1D), bins = np.linspace(0.1,self._a_obs,100), density = True);
        x = .5 * (bins[1:] + bins[:-1])
        self.res['a_LMM'] = x
        self.res['post1D'] = post1D
        self.res['cred_interval_1D'] = credible_interval(x, post1D, conf = 0.68, verbose = self._verbose)
        self.res['BF1D'] = bf(x, post1D, x_cut = x_cut)
        
        if self.__measure_3D:
            if self._verbose:
                print("Sampling 3 sparsity posterior...", end = ' ', flush = True)
            p0 = np.random.randn(nwalkers, ndim)*0.1+0.5
            sampler3D = emcee.EnsembleSampler(nwalkers, ndim, self.loglike3D, vectorize = True)
            state = sampler3D.run_mcmc(p0, 100)
            sampler3D.reset()
            run3D = sampler3D.run_mcmc(state, nsteps)
            self._samples3D = sampler3D.get_chain(flat = False)
            if self._verbose:
                print("Done!", flush = True)
                
            post3D, bins = np.histogram(np.hstack(self._samples3D), bins = np.linspace(0.1,self._a_obs,100), density = True);
            x = .5 * (bins[1:] + bins[:-1])
            self.res['post3D'] = post3D
            self.res['cred_interval_3D'] = credible_interval(x, post3D, conf = 0.68, verbose = self._verbose)
            self.res['BF3D'] = bf(x, post3D, x_cut = x_cut)
            
        if self._verbose:
            print("All Done!", flush = True)    
        self.__runned = True
        return self.res
    
    def reset_chains(self):
        self.__runned = False
        self._samples1D = np.array([])
        self._samples3D = np.array([])
        self.res = {}
        return None
        
    def get_chains(self):
        '''
        getter function that returns the full MCMC chains used to calculate the posterior distributions. The shape of the output is defined when using the MergerTimer.run method.
        
        Returns:
        ----------
        chain1D: (nsteps, nwalkers, 1 array of floats) the MCMC chain for the 1 sparsity posterior
        chain3D: (nsteps, nwalkers, 1 array of floats) the MCMC chain for the 3 sparsity posterior
        '''
        assert self.__runned, 'You must have run the timer before you can retreive the chains'
        
        if self.__measure_3D:
            return self._samples1D, self._samples3D
        
        return self._samples1D
        
        
    def plot_posteriors(self, plottype = None, x_axis = "a"):
        '''
        Simple plotting function to quickly access posteriors  

        Parameters:
        ----------
        plottype: (str) type of plot desire currently suported types, "semilogx" -- "semilogy" -- "loglog"
        x_axis: (str) specifies variable to us on x_axis suported types, "a" -- "z" -- "t" -- "T"
        
        '''
        assert self.__runned, 'You must run the timer before you can plot its outputs'
        assert x_axis in ("a", "z", "t", "T"), f"Unreckognised axis specifier: '{x_axis}', available options are, 'a', 'z', 't' and 'T' "
        fig, ax = plt.subplots(1,1)
        
        def make_plot(x, P, x_low, x_med, x_high, colour, label):
            plt.plot(x, P, c = colour, label = label)

            if np.where(x == x_med)[0].size > 0:
                i_med = np.where(x == x_med)[0][0]
                y_med = P[i_med]
            else:
                y_med = interp1d(x, P)(x_med)
            p = interp1d(x, P, kind = 'linear', bounds_error = False, fill_value = 0)
            plt.vlines(x_med, ymin = 0, ymax = y_med, color = colour)
            #i_low = np.where(x == x_low)[0][0]
            #i_high = np.where(x == x_high)[0][0]
            y_low = p(x_low)#P[i_low]
            y_high = p(x_high)#P[i_high]
            plt.vlines([x_low, x_high], ymin = 0, ymax = [y_low,y_high], color = colour)
            plt.fill_between(x = np.linspace(x_low,x_high, 500), y1 = p(np.linspace(x_low,x_high, 500)), y2= np.zeros(500), alpha = 0.5, color = colour)
            return None
        
        x = self.res['a_LMM']
        P = self.res['post1D']
        
        x_low, x_med, x_high = self.res['cred_interval_1D']
        if x_axis == 'z':
            z_med = 1/x_med - 1
            z_high = 1/x_low - 1
            z_low = 1/x_high - 1
            z = 1/x - 1
            x = z; x_high = z_high; x_low = z_low; x_med = z_med
            x = np.flip(x)
            P = np.flip(P)
        elif x_axis == 't':
            t_obs = self._clock.t_of_a_fast(self._a_obs)/1e3
            t_med = t_obs - self._clock.t_of_a_fast(x_med)/1e3
            t_high = t_obs - self._clock.t_of_a_fast(x_low)/1e3
            t_low = t_obs - self._clock.t_of_a_fast(x_high)/1e3
            t = t_obs - self._clock.t_of_a_fast(x)/1e3
            x = t; x_high = t_high; x_low = t_low; x_med = t_med
            x = np.flip(x)
            P = np.flip(P)
        elif x_axis == 'T':
            T_med = self._clock.T(x_med,self._a_obs)
            T_high = self._clock.T(x_high,self._a_obs)
            T_low = self._clock.T(x_low,self._a_obs)
            T = np.vectorize(self._clock.T)(x,self._a_obs)
            x = T; x_high = T_high; x_low = T_low; x_med = T_med
        colour = "#d95f02"
        make_plot(x, P, x_low, x_med, x_high, colour, "1 sparsity")
        plt.xlabel(x_axis)
        if self.__measure_3D:
            x = self.res['a_LMM']
            P = self.res['post3D']
            x_low, x_med, x_high = self.res['cred_interval_3D']
            if x_axis == 'z':
                z_med = 1/x_med - 1
                z_high = 1/x_low - 1
                z_low = 1/x_high - 1
                z = 1/x - 1
                x = z; x_high = z_high; x_low = z_low; x_med = z_med
                x = np.flip(x)
                P = np.flip(P)
            elif x_axis == 't':
                plt.xlabel(x_axis+" [Gyr]")
                t_obs = self._clock.t_of_a_fast(self._a_obs)/1e3
                t_med = t_obs - self._clock.t_of_a_fast(x_med)/1e3
                t_high = t_obs - self._clock.t_of_a_fast(x_low)/1e3
                t_low = t_obs - self._clock.t_of_a_fast(x_high)/1e3
                t = t_obs - self._clock.t_of_a_fast(x)/1e3
                x = t; x_high = t_high; x_low = t_low; x_med = t_med
                x = np.flip(x)
                P = np.flip(P)
            elif x_axis == 'T':
                plt.xlabel(x_axis+"($a_\mathrm{LMM}|a_{\mathrm{obs}}$)")
                T_med = self._clock.T(x_med,self._a_obs)
                T_high = self._clock.T(x_high,self._a_obs)
                T_low = self._clock.T(x_low,self._a_obs)
                T = np.vectorize(self._clock.T)(x,self._a_obs)
                x = T; x_high = T_high; x_low = T_low; x_med = T_med
                #x = np.flip(x)
                #P = np.flip(P)
            colour = "#7570b3"
            make_plot(x, P, x_low, x_med, x_high, colour, "3 sparsity")
        if plottype in ('semilogy', 'loglog'):
            plt.yscale('log')
        if plottype in ('semilogx', 'loglog'):
            plt.xscale('log')
        
        plt.legend()
        #plt.show()
        return ax
    
    def latexise_results(self):
        '''
        Output reading function to quickly access confidence intervals in terms of a, x and lookback time, t, from when the halo is observed 

        Parameters:
        ----------
        plottype: (str) type of plot desire currently suported types, "semilogx" -- "semilogy" -- "loglog" 
        
        '''
        x_low, x_med, x_high = self.res['cred_interval_1D']
        
        a_str1d = "$a_\\text{LMM} = "+f"{x_med:.3}^{{+{x_high - x_med:.1}}}_{{{x_low - x_med:.1}}}$"
        
        z_med = 1/x_med - 1
        z_high = 1/x_low - 1
        z_low = 1/x_high - 1
        z_str1d = "$z_\\text{LMM} = "+f"{z_med:.3}^{{+{z_high - z_med:.1}}}_{{{z_low - z_med:.1}}}$"
        
        t_obs = self._clock.t_of_a(self._a_obs)/1e3
        t_med = self._clock.t_of_a(x_med)/1e3
        t_high = self._clock.t_of_a(x_high)/1e3
        t_low = self._clock.t_of_a(x_low)/1e3
        t_str1d = "$t_\\text{LMM} = "+f"{t_obs-t_med:.3}^{{+{t_med - t_low:.1}}}_{{{t_med - t_high:.1}}}$ Gyr"
        
        print("1D estimate:", flush = True)
        print(a_str1d, flush = True)
        print(z_str1d, flush = True)
        print(t_str1d, flush = True)
        
        out = [a_str1d, z_str1d, t_str1d]
        
        if self.__measure_3D:
            x_low, x_med, x_high = self.res['cred_interval_3D']
            
            a_str3d = "$a_\\text{LMM} = "+f"{x_med:.3}^{{+{x_high - x_med:.1}}}_{{{x_low - x_med:.1}}}$"
            
            z_med = 1/x_med - 1
            z_high = 1/x_low - 1
            z_low = 1/x_high - 1
            z_str3d = "$z_\\text{LMM} = "+f"{z_med:.3}^{{+{z_high - z_med:.1}}}_{{{z_low - z_med:.1}}}$"
            
            t_obs = self._clock.t_of_a(self._a_obs)/1e3
            t_med = self._clock.t_of_a(x_med)/1e3
            t_high = self._clock.t_of_a(x_high)/1e3
            t_low = self._clock.t_of_a(x_low)/1e3
            t_str3d = "$t_\\text{LMM} = "+f"{t_obs-t_med:.3}^{{+{t_med - t_low:.1}}}_{{{t_med - t_high:.1}}}$ Gyr"
            
            print("3D estimate:", flush = True)
            print(a_str3d, flush = True)
            print(z_str3d, flush = True)
            print(t_str3d, flush = True)
            out += [a_str3d, z_str3d, t_str3d]
        return out
    
    def Gelman_Rubin(self):
        '''
        Calculates the Gelman_Rubin convergence criterion for the MCMC chains.
        
        Returns:
        ----------
        R: (2,float) or float Gelman_rubin covergence criteria for the 1 and 3 sparsity chains
        '''
        
        assert self.__runned, 'You must run the timer before you can analyse the MCMC chains'
        
        chains = self.get_chains()
        
        if self.__measure_3D:
            R = [0,0]
            for i in range(len(chains)):
                X_mean_i = np.mean(chains[i], axis = 0)
                m = chains[i].shape[1]
                n = chains[i].shape[0]
                W = np.sum((chains[i] - X_mean_i)**2)/(m*(n-1))
                B_n = np.sum((X_mean_i - np.mean(chains[i]))**2)/(m-1)
                V = (n-1)/n * W + B_n
                R[i] = V/W
            return R
        
        X_mean_i = np.mean(chains, axis = 0)
        m = chains.shape[1]
        n = chains.shape[0]
        W = np.sum((chains - X_mean_i)**2)/(m*(n-1))
        B_n = np.sum((X_mean_i - np.mean(chains))**2)/(m-1)
        V = (n-1)/n * W + B_n
        R = V/W
        return R
    
class MergerTimerLite:
    def __init__(self, M200 = None, M500 = None, M2500 = None, dM200 = None, dM500 = None, dM2500 = None, z_obs = 0, clock = None, prior = 'flat-a', verbose = False):
        '''
        MergerTimer class. Used to estimate the epoch of the last ajor merger of a dark matter halo

        Parameters:
        ----------
        M200: (float) Observed mass at an overdensity of 200 (Required)
        M500: (float) Observed mass at an overdensity of 500 (Required)
        M2500: (float) Observed mass at an overdensity of 2500 (Optional)
        z_obs: (float) Observed redshift of the object
        clock: (CosmoClock) CosmoClock coresponding to the cosmological model to be used
        prior: (str) prior type to use, current options include 'flat-a', 'flat-t' and 'flat-z' defaults to 'flat-a'
        verbose: (bool) toogles verbosity of the calculations
        
        Methods:
        ----------
        - interp_params_1D : Interpolates the 1 sparsity likelihood parameters for a given value of a, the scale factor
        
        - interp_params_3D : Interpolates the 3 sparsity likelihood parameters for a given value of a, the scale factor
        
        - loglike_1D : Calculates the 1 sparsity likelihood at scale factor a, incorporating the parameters interpolated at the scale factor of observation
        
        - loglike_3D : Calculates the 3 sparsity likelihood at scale factor a, incorporating the parameters interpolated at the scale factor of observation
        
        - run : Runs the MergerTimer to produce analysed data
        
        - get_chains : Returns MCMC chains in numpy arrays of shape (nsteps, nwalkers, 1)
        
        - plot_posteriors : Quick plotting utility to visualise results
        
        - latexise_results : Quick formating utility to output results in a LaTeX freindly format
        
        - Gelman_Rubin : Gelman-Rubin Convergence criterion to verify MCMC is well convergenged
        '''
        self._verbose = verbose
        assert type(clock) == CosmoClock, "The timer requires a CosmoClock to function"
        assert prior in ('flat-a','flat-z','flat-t'), f"Unknown prior '{prior}', currently suported options are 'flat-a' , 'flat-z' and 'flat-t'"
        if self._verbose:
            print("Initialising Timer", flush = True)
        self._M200 = M200
        self._M500 = M500
        self._M2500 = M2500
        self._dM200 = dM200
        self._dM500 = dM500
        self._dM2500 = dM2500
        self._z_obs = z_obs
        self._a_obs = 1/(1+z_obs)
        self._clock = clock
        self._prior = prior
        if M200 is None or M500 is None:
            if M200 is None and M500 is not None:
                raise ValueError("Please specify at least one value for M200")
            if M200 is not None and M500 is None:
                raise ValueError("Please specify at least one value for M500")
            if M200 is None and M500 is None:
                raise ValueError("Please specify at least one value for M200 and M500")
                
        self._s200_500 = M200/M500
        
        self.__uncertainties_1D = False
        if None not in (dM200, dM500):
            self.__uncertainties_1D = True
            self._ds200_500 = self._s200_500 * np.sqrt((dM200/M200)**2 + (dM500/M500)**2)
        #Setting up grid points and interpolation functions
        if self._verbose:
            print(f"Importing parmater grids and setting up parameter vectors at z = {self._z_obs}", flush = True)
        self.__points, self.__tet_vals1D, self.__tet_vals3D, self.__a_pval, self.__tet_pval = import_parameter_grid(file = package_directory+"/parameter_vectors/Uchuu_parameter_vectors.hdf5")
        
        self._T_grid, self._tet1D_grids, self._a_grid = get_params_z(self._z_obs, self.__points, self.__tet_vals1D, clock = clock, npts = 100)
        self.interp_params1D = interp1d(self._a_grid, self._tet1D_grids, kind = "linear", fill_value = 1e-15, bounds_error = False)
        
        self._T_grid, self._tet3D_grids, self._a_grid = get_params_z(self._z_obs, self.__points, self.__tet_vals3D, clock = clock, npts = 100)
        self.interp_params3D = interp1d(self._a_grid, self._tet3D_grids, kind = "linear", fill_value = 1e-15, bounds_error = False)
        
        self._pval_params = np.array([interp1d(self.__a_pval, self.__tet_pval[:,i], kind = "linear", fill_value = 1e-15, bounds_error = False)(self._a_obs) for i in range(4)])
        
        if M2500 is not None:
            if self._verbose:
                print("M2500 specified allowing 3 sparsity estimation!", flush = True)
            self.__measure_3D = True
            self._s = np.array([M200/M500, M200/M2500, M500/M2500])
            #ds = s*np.sqrt(np.array([(dM200/M200)**2 + (dM500/M500)**2,(dM200/M200)**2 + (dM2500/M2500)**2, (dM500/M500)**2 + (dM2500/M2500)**2]))

            #calculate 3D position
            x_temp = self._s-1
            self._pos = np.array([np.sqrt(np.sum(x_temp**2, axis = 0)), np.arctan(np.sqrt(np.sum(x_temp[:2]**2, axis = 0))/x_temp[2]), np.arctan(x_temp[1]/x_temp[0])])
            self.__uncertainties_3D = False
            if None not in (dM200, dM500, dM2500):
                self.__uncertainties_3D = True
                self._ds = self._s *np.sqrt(np.array([(dM200/M200)**2 + (dM500/M500)**2,(dM200/M200)**2 + (dM2500/M2500)**2, (dM500/M500)**2 + (dM2500/M2500)**2]))
        else:
            if self._verbose:
                print("Only M200 and M500 specified, sticking to 1 sparsity estimation!", flush = True)
            self.__measure_3D = False
        self.res = {}
        if self._verbose:
            print("Timer is ready, use MergerTimer.run() to run the timer", flush = True)
        self.__runned = False
        self._samples1D = np.array([])
        self._samples3D = np.array([])
        return None
        
    def loglike3D(self, a):
        '''
        function that computes the 3 sparsity likelihood at for a last major merger at scale factor(s) a

        Parameters:
        ----------
        a: ((n,) floats) scale factors at which to calculate the likelihood function

        Returns:
        ----------
        L: ((n,) floats) likelihood function sampled at a
        '''
        pos = self._pos
        orig_shape = a.shape
        a = a.flatten()
        L = np.zeros(a.shape)
        cond = (a>0.1) & (a<self._a_obs)
        param_vec = self.interp_params3D(a[cond])
        pp = param_vec[:3,:]
        mumu = param_vec[4:6,:]
        covcov = param_vec[6:,:]
        COVCOV = np.zeros((2,2,a[cond].size))
        COVCOV[0,0,:] = covcov[0,:]; COVCOV[1,1,:] = covcov[2,:]; COVCOV[0,1,:] = covcov[1,:]; COVCOV[1,0,:] = covcov[1,:]
        L[cond] = np.log(calculate_3D_likelihood(pos[0], pos[1], pos[2], pp, mumu, COVCOV))
        L[~cond] = -np.inf
        prior = np.zeros(a.shape)
        if self._prior == 'flat-a':
            prior[cond] = 1
        elif self._prior == 'flat-z':
            prior[cond] = 1/a[cond]/a[cond] * 1/(9 - self._z_obs)
        elif self._prior == 'flat-t':
            t0 = self._clock.t_of_a_fast(1/(1+self._z_obs))
            tmin = self._clock.t_of_a_fast(0.1)
            prior[cond] = 1/(t0 - tmin) * 1/a[cond]/self._clock.Hubble_a(a[cond])
        else:
            raise ValueError(f"Unknown prior '{self._prior}', currently suported options are 'flat-a' , 'flat-z' and 'flat-t'")
        prior[~cond] = np.inf
        L = L * prior
        return L.reshape(orig_shape)
    
    def loglike1D(self, a):
        '''
        function that computes the 1 sparsity likelihood at for a last major merger at scale factor(s) a

        Parameters:
        ----------
        a: ((n,) floats) scale factors at which to calculate the likelihood function

        Returns:
        ----------
        L: ((n,) floats) likelihood function sampled at a
        '''
        a = np.array(a)
        orig_shape = a.shape
        cond = (a>0.1) & (a<self._a_obs)
        L = np.zeros(a.shape)
        param_vec = self.interp_params1D(a[cond])
        L[cond] = np.log(gen_beta_prime(self._s200_500-1, *param_vec))
        prior = np.ones(a.size)
        L[~cond] = -np.inf
        prior = np.zeros(a.shape)
        if self._prior == 'flat-a':
            prior[cond] = 1
        elif self._prior == 'flat-z':
            prior[cond] = 1/a[cond]/a[cond] * 1/(9 - self._z_obs)
        elif self._prior == 'flat-t':
            t0 = self._clock.t_of_a_fast(1/(1+self._z_obs))
            tmin = self._clock.t_of_a_fast(0.1)
            prior[cond] = 1/(t0 - tmin) * 1/a[cond]/self._clock.Hubble_a(a[cond])
        else:
            raise ValueError(f"Unknown prior '{self._prior}', currently suported options are 'flat-a' , 'flat-z' and 'flat-t'")
        prior[~cond] = np.inf
        L = L * prior
        return L.reshape(orig_shape)
    
    def set_masses(self, M200 = None, M500 = None, M2500 = None, dM200 = None, dM500 = None, dM2500 = None):
        self._M200 = M200
        self._M500 = M500
        self._M2500 = M2500
        self._dM200 = dM200
        self._dM500 = dM500
        self._dM2500 = dM2500
        self._s200_500 = M200/M500
        
        self.__uncertainties_1D = False
        if None not in (dM200, dM500):
            self.__uncertainties_1D = True
            self._ds200_500 = self._s200_500 * np.sqrt((dM200/M200)**2 + (dM500/M500)**2)
        
        if M2500 is not None:
            if self._verbose:
                print("M2500 specified allowing 3 sparsity estimation!", flush = True)
            self.__measure_3D = True
            self._s = np.array([M200/M500, M200/M2500, M500/M2500])

            #calculate 3D position
            x_temp = self._s-1
            self._pos = np.array([np.sqrt(np.sum(x_temp**2, axis = 0)), np.arctan(np.sqrt(np.sum(x_temp[:2]**2, axis = 0))/x_temp[2]), np.arctan(x_temp[1]/x_temp[0])])
            self.__uncertainties_3D = False
            if None not in (dM200, dM500, dM2500):
                self.__uncertainties_3D = True
                self._ds = self._s *np.sqrt(np.array([(dM200/M200)**2 + (dM500/M500)**2,(dM200/M200)**2 + (dM2500/M2500)**2, (dM500/M500)**2 + (dM2500/M2500)**2]))
        else:
            if self._verbose:
                print("Only M200 and M500 specified, sticking to 1 sparsity estimation!", flush = True)
            self.__measure_3D = False
        return None
            
    def set_redshift(self, z_obs):
        self._z_obs = z_obs
        self._a_obs = 1/(1+z_obs)
        if self._verbose:
            print(f"Importing parmater grids and setting up parameter vectors at z = {self._z_obs}", flush = True)
            
        self.__points, self.__tet_vals1D, self.__tet_vals3D, self.__a_pval, self.__tet_pval = import_parameter_grid(file = package_directory+"/parameter_vectors/Uchuu_parameter_vectors.hdf5")
        
        self._T_grid, self._tet1D_grids, self._a_grid = get_params_z(self._z_obs, self.__points, self.__tet_vals1D, clock = clock, npts = 100)
        self.interp_params1D = interp1d(self._a_grid, self._tet1D_grids, kind = "linear", fill_value = 1e-15, bounds_error = False)
        
        self._T_grid, self._tet3D_grids, self._a_grid = get_params_z(self._z_obs, self.__points, self.__tet_vals3D, clock = clock, npts = 100)
        self.interp_params3D = interp1d(self._a_grid, self._tet3D_grids, kind = "linear", fill_value = 1e-15, bounds_error = False)
        
        self._pval_params = np.array([interp1d(self.__a_pval, self.__tet_pval[:,i], kind = "linear", fill_value = 1e-15, bounds_error = False)(self._a_obs) for i in range(4)])
        return None
    
    def run(self, nwalkers = 10, nsteps = 10000):
        '''
        Initiates MCMC sampling of the posterior distribtuion function(s)  

        Parameters:
        ----------
        nwalker: (int) number of random walkers for the MCMC chains
        nsteps: (int) number of steps made by each walker

        Returns:
        ----------
        res: (dict) ditionary containing the estimated posterior likelyhood function(s) and sample points.
        Once calculated this distionarry can be accessed as an atribute of the MergerTimer
        '''
        
        if self._verbose:
            print("Sampling 1 sparsity posterior...", end = ' ', flush = True)
        ndim = 1
        p0 = np.random.randn(nwalkers, ndim)*0.1+0.5
        sampler1D = emcee.EnsembleSampler(nwalkers, ndim, self.loglike1D, vectorize = True)
        state = sampler1D.run_mcmc(p0, 100)
        sampler1D.reset()
        run1D = sampler1D.run_mcmc(state,nsteps)
        self._samples1D = sampler1D.get_chain(flat = False)
        
        if self._verbose:
            print("Done!", flush = True)
            
        x_cut = root(lambda x: self._clock.T(x,self._a_obs) + 2, x0 = 0.5)['x'][0]
        post1D, bins = np.histogram(np.hstack(self._samples1D), bins = np.linspace(0.1,self._a_obs,100), density = True);
        x = .5 * (bins[1:] + bins[:-1])
        self.res['a_LMM'] = x
        self.res['post1D'] = post1D
        self.res['cred_interval_1D'] = credible_interval(x, post1D, conf = 0.68, verbose = self._verbose)
        #self.res['BF1D'] = bf(x, post1D, x_cut = x_cut)
        
        if self.__measure_3D:
            if self._verbose:
                print("Sampling 3 sparsity posterior...", end = ' ', flush = True)
            p0 = np.random.randn(nwalkers, ndim)*0.1+0.5
            sampler3D = emcee.EnsembleSampler(nwalkers, ndim, self.loglike3D, vectorize = True)
            state = sampler3D.run_mcmc(p0, 100)
            sampler3D.reset()
            run3D = sampler3D.run_mcmc(state, nsteps)
            self._samples3D = sampler3D.get_chain(flat = False)
            if self._verbose:
                print("Done!", flush = True)
                
            post3D, bins = np.histogram(np.hstack(self._samples3D), bins = np.linspace(0.1,self._a_obs,100), density = True);
            x = .5 * (bins[1:] + bins[:-1])
            self.res['post3D'] = post3D
            self.res['cred_interval_3D'] = credible_interval(x, post3D, conf = 0.68, verbose = self._verbose)
            #self.res['BF3D'] = bf(x, post3D, x_cut = x_cut)
            
        if self._verbose:
            print("All Done!", flush = True)    
        self.__runned = True
        return self.res
    
    def get_chains(self):
        '''
        getter function that returns the full MCMC chains used to calculate the posterior distributions. The shape of the output is defined when using the MergerTimer.run method.
        
        Returns:
        ----------
        chain1D: (nsteps, nwalkers, 1 array of floats) the MCMC chain for the 1 sparsity posterior
        chain3D: (nsteps, nwalkers, 1 array of floats) the MCMC chain for the 3 sparsity posterior
        '''
        assert self.__runned, 'You must have run the timer before you can retreive the chains'
        
        if self.__measure_3D:
            return self._samples1D, self._samples3D
        
        return self._samples1D
        
    
    def Gelman_Rubin(self):
        '''
        Calculates the Gelman_Rubin convergence criterion for the MCMC chains.
        
        Returns:
        ----------
        R: (2,float) or float Gelman_rubin covergence criteria for the 1 and 3 sparsity chains
        '''
        
        assert self.__runned, 'You must run the timer before you can analyse the MCMC chains'
        
        chains = self.get_chains()
        
        if self.__measure_3D:
            R = [0,0]
            for i in range(len(chains)):
                X_mean_i = np.mean(chains[i], axis = 0)
                m = chains[i].shape[1]
                n = chains[i].shape[0]
                W = np.sum((chains[i] - X_mean_i)**2)/(m*(n-1))
                B_n = np.sum((X_mean_i - np.mean(chains[i]))**2)/(m-1)
                V = (n-1)/n * W + B_n
                R[i] = V/W
            return R
        
        X_mean_i = np.mean(chains, axis = 0)
        m = chains.shape[1]
        n = chains.shape[0]
        W = np.sum((chains - X_mean_i)**2)/(m*(n-1))
        B_n = np.sum((X_mean_i - np.mean(chains))**2)/(m-1)
        V = (n-1)/n * W + B_n
        R = V/W
        return R
    
    def reset_chains(self):
        self.__runned = False
        self._samples1D = np.array([])
        self._samples3D = np.array([])
        self.res = {}
        return None