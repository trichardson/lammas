# LAMMAS

LAst Major Merger Age with Sparsity

By T. R. G. Richardson

Thank you for using LAMMAS.

This simple Python library uses sparsity estimate the time of the last major merger of a dark matter halo, see Richardson & Corasaniti (2021) [https://arxiv.org/abs/2112.04926] for details. If you use LAMMAS for any scientific publications please reffer to the aforementioned publication.

For instructions on how to use LAMMAS please reffer to the example notebook.

dependencies: 
- numpy
- scipy
- matplotlib
- emcee (v. > 3.1.x)
- h5py

